import glob

import python.train as train
import python.corpus as corpus


# Source: UMass CS685 (Advanced NLP): Implementing a neural language model in PyTorch
#  https://www.youtube.com/watch?v=pZt2TLSl7L4:

if __name__ == '__main__':
    list_of_sigla = ["Phil_U", "Sal_J", "Mad_A", "Mad_B", "Sev_Z", "Sev_R", "Esc_Q", "Mad_G"]
    #list_of_sigla = ["Mad_G"]
    list_of_xml_files = [f"/home/mgl/Bureau/These/Edition/collator/temoins_tokenises_regularises/{sigla}.xml"
                     for sigla in list_of_sigla]

    list_of_txt_files = glob.glob("/home/mgl/Bureau/These/datasets/embeddings/corpus/lemmatise/*.txt")

    window_size = 6
    corpus_xml = corpus.Corpus(window_size=window_size)
    corpus_xml.import_xml_corpus(list_of_xml_files)
    corpus_xml.import_txt_corpus(list_of_txt_files)
    corpus_xml.create_examples()
    corpus_xml.create_vocab()
    corpus_xml.create_dataset([90, 10])

    # corpus += import_txt_corpus("/home/mgl/Dropbox/corpus/alfonso_GeneralEstoriaPrimera_2009/texto_bruto"
    # "/full_text_lemmatized.txt")
    trainer = train.Trainer(train_inputs=corpus_xml.train_examples,
                            test_inputs=corpus_xml.test_examples,
                            epochs=30,
                            lr=0.2,
                            batch_size=64,
                            device="cuda:0",
                            dimension_embs=150,
                            dimension_hidden=256,
                            window_size=window_size,
                            vocab=corpus_xml.vocab,
                            dropout=0.1)

    trainer.train(shuffle_examples=True)
    trainer.save_state_dict()
