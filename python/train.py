#!/usr/bin/env python
# coding: utf-8
import json
import random

import numpy as np
import torch
import torch.nn as nn
import tqdm

import python.network as network


# Source: UMass CS685 (Advanced NLP): Implementing a neural language model in PyTorch
#  https://www.youtube.com/watch?v=pZt2TLSl7L4:

class Trainer:
    def __init__(self, train_inputs, test_inputs, epochs, lr, batch_size, device, dimension_embs, dimension_hidden, window_size, vocab, dropout):
        self.epochs = epochs
        self.length_test_sample = round(0.005 * len(train_inputs+test_inputs))
        print(f"Length of test sample: {self.length_test_sample}")
        self.lr = lr
        self.criterion = nn.CrossEntropyLoss()
        self.batch_size = batch_size
        self.steps = len(train_inputs) // self.batch_size
        print(len(train_inputs))
        self.device = device
        self.network = network.NLM(
            d_embedding=dimension_embs,
            d_hidden=dimension_hidden,
            window_size=window_size,
            len_vocab=len(vocab),
            dropout=dropout
        )
        self.optimizer = torch.optim.SGD(params=self.network.parameters(), lr=self.lr)
        self.network.to(torch.device(self.device))
        print(train_inputs[:10])
        self.train_examples = torch.LongTensor([sentence[:-1] for sentence in train_inputs])
        self.train_labels = torch.LongTensor([sentence[-1] for sentence in train_inputs])
        self.test_examples = torch.LongTensor([sentence[:-1] for sentence in test_inputs])
        self.test_labels = torch.LongTensor([sentence[-1] for sentence in test_inputs])
        self.best_state_model = None
        self.accuracy = 0
        self.previous_accuracy = 0
        self.accuracy_list = []

    def train(self, shuffle_examples=True):
        """
        :param shuffle_examples: Faut-il mélanger le jeu de données entre chaque epoch ?
        """
        self.eval_init()
        for i in range(self.epochs):
            if shuffle_examples:
                idx = torch.randperm(self.train_examples.shape[0])
                self.train_examples = self.train_examples[idx, :]
                self.train_labels = self.train_labels[idx]
            epoch_losses = []
            for j in tqdm.tqdm(range(self.steps)):
                current_examples = self.train_examples[j * self.batch_size:(j + 1) * self.batch_size].to(self.device)
                current_labels = self.train_labels[j * self.batch_size:(j + 1) * self.batch_size].to(self.device)
                logits = self.network(current_examples)
                loss = self.criterion(logits, current_labels)
                epoch_losses.append(loss.item())
                # now let's update our params to make the loss smaller
                # step 1: compute gradient (partial derivs of loss WRT parameters)
                loss.backward()

                # step 2: update params using gradient descent
                self.optimizer.step()

                # step 3: zero out the gradients for the next epoch
                self.optimizer.zero_grad()
            mean_loss = np.mean(epoch_losses)
            print(f"Epoch: {i + 1}, train loss: {mean_loss}")
            self.eval()
            self.accuracy_list.append(self.accuracy)
            if self.previous_accuracy > self.accuracy:
                self.best_state_model = self.network.state_dict()
            self.previous_accuracy = self.accuracy

    def eval(self):
        """
        Cette fonction évalue la qualité d'un modèle à un moment t sur 1000 exemples
        tirés du jeu de test.
        """
        print("Evaluation on test data")
        random_number = random.randint(self.length_test_sample, (len(self.test_examples) - self.length_test_sample + 1))
        test_examples_sample = self.test_examples[random_number:random_number+self.length_test_sample].to(self.device)
        test_labels_sample = self.test_labels[random_number:random_number+self.length_test_sample].to(self.device)
        with torch.no_grad():
            predictions = self.network(test_examples_sample)
        loss = self.criterion(predictions, test_labels_sample)

        predicted_index = torch.topk(predictions, 1)
        list_of_indices = [tensor[0].item() for tensor in list(predicted_index.indices.data)]
        labels = [label.item() for label in test_labels_sample]
        predictions_labels = list(zip(list_of_indices, labels))
        self.accuracy = 0
        for pred, gt in predictions_labels:
            if pred == gt:
                self.accuracy += 1
        pourcentage = round((self.accuracy / self.length_test_sample) * 100, 1)
        print(f"Accuracy: {self.accuracy} out of {self.length_test_sample} test samples ({pourcentage}%)\n"
              f"Loss on test set: {loss}")

    def eval_init(self):
        """
        Cette fonction évalue la qualité d'un modèle à un moment t sur 1000 exemples
        tirés du jeu de test.
        """
        print("Evaluation on test data")
        random.seed(2345)
        sample_length = 1000
        random_number = random.randint(sample_length, (len(self.train_examples) - sample_length + 1))
        train_examples_sample = self.train_examples[random_number:random_number+sample_length].to(self.device)
        train_labels_sample = self.train_labels[random_number:random_number+sample_length].to(self.device)
        with torch.no_grad():
            predictions = self.network(train_examples_sample)
        loss = self.criterion(predictions, train_labels_sample)
        print(f"Initial train loss: {loss}")

    def save_state_dict(self):
        print(f"Saving best model. Best accuracy: {max(self.accuracy_list)} "
              f"for epoch {self.accuracy_list.index(max(self.accuracy_list)) + 1}.")
        torch.save(self.network.state_dict(), "modeles/state_dict_embeddings.pt")

    def save_model(self):
        torch.save(self.network, "modeles/model_embeddings.pt")



