import json
import random
from lxml import etree


class Corpus:
    def __init__(self, window_size: int):
        self.corpus = list()
        self.window_size = window_size
        self.trainable_examples = None
        self.splitted_corpus = None
        self.test_examples = list()
        self.train_examples = list()
        self.vocab = dict()
        self.inputs = list()

    def import_xml_corpus(self, files):
        for file in files:
            self.corpus += self.import_xml_file(file)

    def import_txt_corpus(self, files):
        for file in files:
            self.corpus += self.import_txt_file(file)

    def import_xml_file(self, file, normalize=True):
        with open(file, 'r') as xml_file:
            f = etree.parse(xml_file)
        tei_namespace = "http://www.tei-c.org/ns/1.0"
        NSMAP = dict(tei=tei_namespace)
        words_list = f.xpath("//tei:w[ancestor::tei:p]", namespaces=NSMAP)
        output_list = []
        # On ne va pas directement nourir avec les formes, mais on va profiter plutôt des analyses.
        # Suppose de donner à manger la même chose à l'algorithme lors de la prédiction.
        if normalize:
            for word in words_list:
                pos = word.xpath("@pos")[0]
                lemma = word.xpath("@lemma")[0]
                output_list.append(f"{pos}{lemma}")
        else:
            output_list = [word.text for word in words_list]
        return output_list

    def import_txt_file(self, file, normalize=True):
        with open(file, 'r') as txt_file:
            f = [line.split(" ") for line in txt_file.read().split("\n") if len(line) > 3]
        if normalize:
            try:
                output_list = [f"{pos}{lemma}" for form, lemma, pos, *other in f[:-1]]
            except:
                print(f)
            # On élimine la ponctuation
            output_list = [analyse for analyse in output_list if not analyse.startswith("F")]
        else:
            output_list = [form for form, *other in f]
        return output_list

    def create_examples(self, shuffle_examples=True):
        splitted_corpus = [self.corpus[i:i + self.window_size + 1]
                           for i in range(len(self.corpus))]
        self.splitted_corpus = [sentence for sentence in splitted_corpus
                                if len(sentence) == self.window_size + 1]
        if shuffle_examples:
            random.shuffle(self.splitted_corpus)

    def create_vocab(self):
        # Dans notre cas, la longueur du vocabulaire correspond à la taille de nombre de mots
        # différents du corpus après normalisation graphique (=analyse lexico-grammaticale)

        for sentence in self.splitted_corpus:
            sent_idxes = []
            for w in sentence:
                if w not in self.vocab:
                    self.vocab[w] = len(self.vocab)  # add a new type to the vocab
                sent_idxes.append(self.vocab[w])
            self.inputs.append(sent_idxes)
        self.vocab["~"] = len(self.vocab)
        print(f"Length of vocab: {len(self.vocab)}")

    def create_dataset(self, proportion: list):
        """
        :proportion: Une liste avec les proportions en train et en test: [80, 20] par exemple
        """
        try:
            sum(proportion) == 100
        except ValueError as _:
            print("The sum of the proportion must be 100")
            exit(0)

        train_proportion = round(len(self.splitted_corpus) * (proportion[0] / 100))
        self.train_examples = self.inputs[:train_proportion]
        self.test_examples = self.inputs[train_proportion:]
        self.save_inputs()

    def save_inputs(self):
        with open("data/data.json", "w") as data_file:
            json.dump(self.splitted_corpus, data_file)
