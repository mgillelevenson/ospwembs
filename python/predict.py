import json

import torch
import torch.nn as nn
from lxml import etree
import random
device = "cpu"


def import_corpus(file, normalize=True):
    with open(file, 'r') as xml_file:
        f = etree.parse(xml_file)
    tei_namespace = "http://www.tei-c.org/ns/1.0"
    NSMAP = dict(tei=tei_namespace)
    words_list = f.xpath("//tei:w[ancestor::tei:p]", namespaces=NSMAP)
    output_list = []
    # On ne va pas directement nourir avec les formes, mais on va profiter plutôt des analyses.
    # Suppose de donner à manger la même chose à l'algorithme lors de la prédiction.
    if normalize:
        for word in words_list:
            pos = word.xpath("@pos")[0]
            lemma = word.xpath("@lemma")[0]
            output_list.append(f"{pos}{lemma}")
    else:
        output_list = [word.text for word in words_list]
    return output_list


corpus = import_corpus("/home/mgl/Bureau/These/Edition/collator/temoins_tokenises_regularises/Sal_J.xml",
                       normalize=False)

window_size = 5

total_number_of_groups = len(corpus) // window_size

splitted_corpus = [corpus[i:i + window_size + 1]
                   for i in range(len(corpus))]

splitted_corpus = [sentence for sentence in splitted_corpus if len(sentence) == window_size + 1]

with open("vocab.json", "r") as vocab_json:
    vocab = json.load(vocab_json)



def tensor_to_sentence(tensor):
    tensor = tensor.tolist()[0]
    print(tensor)
    sentence = "Sentence:" + " ".join(
        [key for key, value in vocab.items() for current_value in tensor if value == current_value])
    print(sentence)


def sentence_to_tensor(sentence, tokenized=False):
    if not tokenized:
        sentence_as_list = sentence.split()
    else:
        sentence_as_list = sentence
    print(sentence_as_list)
    sentence_as_indexes = []
    for word in sentence_as_list:
        if word in vocab:
            sentence_as_indexes.append(vocab[word])
        else:
            sentence_as_indexes.append(vocab["~"])

    sentence_as_indexes = torch.LongTensor(sentence_as_indexes)
    return sentence_as_indexes


def compare_sentences(a, b):
    '''
    L'idée est ici de comparer deux propositions avec variation.
    La variation doit concerner le dernier mot.
    Ici est pris en compte le contexte.
    '''


    first_sentence = a.split()
    target_a = first_sentence.pop()

    second_sentence = b.split()
    target_b = second_sentence.pop()


    sentence_a_as_tensor = sentence_to_tensor(first_sentence, tokenized=True).unsqueeze(0).to(device)


    logits = network(sentence_a_as_tensor)

    rev_vocab = dict((idx, word) for (word, idx) in vocab.items())
    norm_vocab = dict((word, idx) for (word, idx) in vocab.items())

    probs = nn.functional.softmax(logits, dim=1).squeeze()

    all_probs = torch.topk(probs, len(vocab))
    probs_as_indices = dict(zip(all_probs.indices.tolist(), all_probs.values.tolist()))

    ordered_indices = all_probs.indices.tolist()

    prob_first_word = probs_as_indices[norm_vocab[target_a]]
    prob_second_word = probs_as_indices[norm_vocab[target_b]]

    index_first_word = ordered_indices[norm_vocab[target_a]]
    index_second_word = ordered_indices[norm_vocab[target_b]]
    print(f"Predicted word: {rev_vocab[ordered_indices[0]]}")
    print(prob_first_word)
    print(prob_second_word)
    print(index_first_word, index_second_word)
    print(f"{abs(index_first_word - index_second_word)} mots prédits séparent ces deux mots")


def cosine_similarity(embbedings, a, b):
    """
    a et b sont les deux mots que l'on veut comparer.
    """

    norm_vocab = dict((word, idx) for (word, idx) in vocab.items())
    rev_vocab = dict((idx, word) for (word, idx) in vocab.items())
    a_as_vector = embbedings[norm_vocab[a]]
    b_as_vector = embbedings[norm_vocab[b]]

    print(len(a_as_vector), len(b_as_vector))
    cosine_metric = torch.dot(a_as_vector, b_as_vector)

    # We compare to two random words
    test_word_a_index = random.randint(0, len(vocab) - 1)
    test_word_b_index = random.randint(0, len(vocab) - 1)
    test_word_a = rev_vocab[test_word_a_index]
    test_word_b = rev_vocab[test_word_b_index]
    test_a_as_vector = embbedings[test_word_a_index]
    test_b_as_vector = embbedings[test_word_b_index]
    test_cosine_metric = torch.dot(test_a_as_vector, test_b_as_vector)
    print(f"{a}, {b}")
    print(f"La similarité est: {cosine_metric}. "
          f"Version de pytorch: {print(torch.__version__)}"
          f"Même métrique sur deux mots choisis au hasard, '{test_word_a}' et '{test_word_b}': {test_cosine_metric}\n")

network = torch.load("model_embeddings.pt")

# let's first define a reverse vocabulary mapping (idx>word type)
def test():
    sentence = "E dixo Alexandre a sus"
    original_sentence = sentence.lower()
    rev_vocab = dict((idx, word) for (word, idx) in vocab.items())
    sentence_as_tensor = sentence_to_tensor(sentence).unsqueeze(0).to(device)
    logits = network(sentence_as_tensor)
    probs = nn.functional.softmax(logits, dim=1).squeeze()

    higher_probs = torch.topk(probs, 3)
    probs_as_indices = list(zip(higher_probs.values.tolist(), higher_probs.indices.tolist()))
    higher_probabilities = "\n".join([f'{rev_vocab[index]} ({prob})' for prob, index in probs_as_indices])

    print(f"Given '{sentence}', the model predicts: \n{higher_probabilities}")

# compare_sentences("sentençia deste capítulo está en dos", "sentençia deste capítulo está en tres")

# Get embeddings:
embedding = network.embeddings.weight
print(embedding)
cosine_similarity(embedding.data, "DA0MP0el", "DP3CPNsu")

