import torch.nn as nn


class NLM(nn.Module):
    # two things that you need to do
    # 1. init function (initializes all the **parameters** of the network)
    # 2. forward function (defines the forward propagation computations)

    def __init__(self, d_embedding, d_hidden, window_size, len_vocab, dropout=0.5):
        super(NLM, self).__init__() # init the base Module class
        self.d_emb = d_embedding
        self.embeddings = nn.Embedding(len_vocab, d_embedding)
        self.Dropout = nn.Dropout(p=dropout, inplace=False)

        # concatenated embeddings > hidden
        self.W_hid = nn.Linear(d_embedding*window_size, d_hidden)
        self.activation_function = nn.ReLU()
        # hidden > output probability distribution over vocab
        self.W_out = nn.Linear(d_hidden, len_vocab)

    def forward(self, input): # each input will be a batch of prefixes (in this case 4)
        batch_size, window_size = input.size()
        embs = self.embeddings(input) # 4 x 2 x 5
        # print('embedding size:', embs.size())
        # next,  we want to concatenate the prefix embeddings together
        concat_embs = embs.view(batch_size, window_size * self.d_emb)
        # print('concatenated embs size:', concat_embs.size())

        # now, we project this to the hidden space
        hiddens = self.W_hid(concat_embs)
        # print('hidden size:', hiddens.size())
        activated = self.activation_function(hiddens)
        # finally, project hiddens to vocabulary space
        outs = self.W_out(self.Dropout(activated))
        # print('output size:', outs.size())

        return outs # return unnormalized probability, also known as **logits**

